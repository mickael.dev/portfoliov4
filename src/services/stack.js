// Need to use the React-specific entry point to allow generating React hooks
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

// Define a service using a base URL and expected endpoints
export const stackApi = createApi({
  reducerPath: 'stackApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://oey3potr.directus.app/items/' }),
  endpoints: (builder) => ({
    getStack: builder.query({
      query: () => '/stack',
    }),
  }),
})

// Export hooks for usage in function components, which are
// auto-generated based on the defined endpoints
export const { useGetStackQuery } = stackApi