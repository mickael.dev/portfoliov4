import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const contentsApi = createApi({
  reducerPath: 'contentsApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://oey3potr.directus.app/items/' }),
  endpoints: (builder) => ({
    getContents: builder.query({
      query: () => '/contents',
    }),
  }),
})


export const { useGetContentsQuery } = contentsApi