import { useEffect } from "react";

const AboutMe = ({ title, content }) => {


    useEffect(() => {
        const aboutMeObserver = new IntersectionObserver((entries) => {
            entries.forEach(entry => {
                let container = entry.target;
                let content = container.children[1];
                if(entry.isIntersecting) {
                    content.classList.add("showInWithBlur");
                } else {
                    content.classList.remove("showInWithBlur");
                }
            })
        })
        aboutMeObserver.observe(document.querySelector(".AboutMe"));
    }, [])

    return (
        <div className="AboutMe">
            <img className="AboutMe__img" src="./img/aboutMe.jpg" alt="about me"/>
            <div className="AboutMe__content opacity-0" id="AboutMe">
                <h2 className="text-h2 mb-10">{title}</h2>
                <p class="font-fira-code">{content}</p>
                <a className="button" href="mailto:contact@mickael-grass.com">Contact me</a>
                <a className="malt" href="https://www.malt.fr/profile/mickaelgrass">
                    <span>Join me on </span>
                    <img src="./img/logoMalt.png" alt="Malt logo"/>
                </a>
            </div>
            <div className="AboutMe__blurredbg"></div>
        </div>
    )
}

export default AboutMe;