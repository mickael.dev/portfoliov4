import { useGetStackQuery } from "../../services/stack";

const Stack = ({ title, subtitle }) => {

    const { data, isFetching } = useGetStackQuery();

    return (
        <div id="stack" className="flex items-center justify-center mt-10 flex-wrap w-1/2 gap-3">
                { data && data.data.map((stack, index) => (
                    <a href={stack.URL} key={stack.id} className="cursor-pointer bg-main-blue-opacity-20 text-main-blue transition-all hover:text-sky-400 rounded-full px-4 py-2" > 
                        <p className="text-2xl">{stack.name}</p>
                    </a>
                ))}
                {isFetching && <p>Fetching data...</p>}
        </div>
    )
}

export default Stack;