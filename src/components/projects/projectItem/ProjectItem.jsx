const ProjectItem = ({ projectData }) => {
    return (
        <a href={projectData.URL} target="blank"
        className={"ProjectItem transition-all " + projectData.name} 
        id={projectData.name} 
        data-index-number={projectData.id} 
        >
            <h2 className="ProjectItem__title"> {projectData.name} </h2>
            <div className="ProjectItem__description"> {projectData.description} </div>
            <img className="ProjectItem__img" src={"https://oey3potr.directus.app/assets/" + projectData.img} alt={projectData.description}/>
            <div className="ProjectItem__tags"><span>made with</span>
                    { projectData.tag.map((tag, index) => (
                        <div key={index} className={"ProjectItem__tags__tag " + tag}>{tag}</div>
                        ))}
            </div>
        </a>
    )
}

export default ProjectItem;