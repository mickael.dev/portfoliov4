import React from "react";
import ProjectItem from "./projectItem/ProjectItem";
import { useGetProjectsQuery } from "../../services/projects";
import { BsFillArrowRightCircleFill, BsFillArrowLeftCircleFill } from "react-icons/bs";

const ProjectContainer = ({theme}) => {
    const { data, isFetching } = useGetProjectsQuery();
    return (
        <div className={"ProjectContainer section -mt-24 relative z-30 " + theme} id="projects">
            <div id="projectContainer" className={"ProjectContainer__box group max-w-max px-20 " + theme}>
                { data && !isFetching &&data.data.map((projectData, index) => (
                    <ProjectItem id={projectData.id} key={index} theme={theme} projectData={projectData}/>
                ))}
                <div className="group-hover:opacity-100 transition-all top-0 left-0 absolute w-screen flex justify-between h-full px-14 items-center">
                    <BsFillArrowLeftCircleFill className="cursor-pointer text-main-blue text-5xl transition-all hover:opacity-50 hover:text-white"/>
                    <BsFillArrowRightCircleFill className="cursor-pointer text-main-blue text-5xl transition-all hover:opacity-50 hover:text-white"/>
                </div>
            </div>
        </div>
    )
}

export default ProjectContainer;

