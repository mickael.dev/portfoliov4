import { SiReact, SiSass } from "react-icons/si";
const Footer = ({ madeWith }) => {
    return (
        <footer className="Footer">
            <div className="Footer__box">
                <div className="Footer__text">Made with </div>
                <div className="Footer__icons">
                    <SiReact />
                    <SiSass />
                </div>
                <div class="Footer__madeWith">
                    <p>Back office: </p>
                    <a href="https://directus.io/">
                        <img height="75" src={"https://oey3potr.directus.app/assets/" + madeWith } alt="Directus"/>
                    </a>
                </div>
            </div>
        </footer>
    )
}

export default Footer;