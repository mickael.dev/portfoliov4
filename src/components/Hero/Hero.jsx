import React from "react";
import { FaGitlab, FaGithub } from "react-icons/fa";
import Stack from '../Stack/Stack';

const Hero = (
    {
        theme,
        title,
        subtitle,
        Github, 
        Gitlab,
        video
    }) => {
    
    return (
        <section className={"relative max-w-screen-2xl h-screen z-10 " + theme} id="Hero">
            <div className={"Hero__presentation_box opacity-0" + theme}>
                <video className={theme === "light" ? "invert absolute top-0 left-0 w-screen h-screen z-0" : "absolute top-0 left-0 w-screen h-screen z-0"} autoPlay muted loop src={"https://oey3potr.directus.app/assets/" + video}></video>
                <div className={theme === "light" ? "invert absolute top-0 left-0 w-screen h-screen z-20 bg-opacity-50 bg-black" : "absolute top-0 left-0 w-screen h-screen z-20 bg-opacity-50 bg-black"}></div>
                <div id="top" className="relative z-20 px-16 h-screen flex flex-col justify-center items-center">
                    <h1 className="relative text-center text-h1">
                        {title}
                    </h1>
                    <div className=" text-5xl mb-20 font-light font-fira-code">{subtitle}</div>
                    <div className="flex items-center flex-col">
                        <div className="mb-10 flex flex-col items-center">
                            <a href="#projects" className="bg-main-blue text-white text-body1 transition-all border-main-blue hover:bg-transparent hover:text-main-blue border rounded-full py-4 px-8 mb-10">View my projects</a>
                            <a href="#AboutMe" className="text-main-blue text-body1 transition-all hover:text-white">More about me</a>
                        </div>
                        <div className="flex items-center h-full">
                            <a href={Github} className="github flex items-center text-3xl transition-all hover:opacity-50 mr-10">
                                <FaGithub /> 
                                <span className="ml-3">Github</span>
                            </a>
                            <a href={Gitlab} className="gitlab flex items-center text-3xl transition-all hover:opacity-50">
                                <FaGitlab /> 
                                <span className="ml-3">Gitlab</span>
                            </a>
                        </div>
                    </div>
                    <Stack />
                </div>
            </div>
        </section>
    )
}

export default Hero;