
import ProjectsContainer from './components/projects/ProjectsContainer';
import './assets/scss/App.scss';
import React from 'react';
import Hero from './components/Hero/Hero';
import Header from './components/Header/Header';
import AboutMe from './components/aboutMe/AboutMe';
import Footer from './components/Footer/Footer';
import useDarkMode from './customHook/useDarkMode';
import ToggleColorMode from './components/Header/ToggleColorMode';
import { useGetContentsQuery } from './services/contents';

function App() {
  const {theme, toggleTheme} = useDarkMode();
    const { data, isFetching } = useGetContentsQuery();

    if(isFetching) {

    } else {
        return (
            <div className={"App " + theme}>
                <ToggleColorMode 
                toggleTheme={toggleTheme} 
                theme={theme} />
                <Header />
                <Hero 
                    title={data.data.map(data => data.title)}
                    subtitle={data.data.map(data => data.subtitle)}
                    Github={data.data.map(data => data.Github)}
                    Gitlab={data.data.map(data => data.Gitlab)}
                    theme={theme} 
                    video={data.data.map(data => data.Video)} 
                />
                <ProjectsContainer theme={theme}/>
                <AboutMe 
                    title={data.data.map(data => data.about_me_title)}
                    content={data.data.map(data => data.about_me_content)}
                />
                <Footer madeWith={data.data.map(data => data.madeWith)}/>
            </div>
        );
    }

}

export default App;
