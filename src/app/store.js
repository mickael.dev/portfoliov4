import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import { projectsApi } from '../services/projects';
import { stackApi } from '../services/stack';
import { contentsApi } from '../services/contents';
export const store = configureStore({
  reducer: {
    [projectsApi.reducerPath]: projectsApi.reducer,
    [stackApi.reducerPath]: stackApi.reducer,
    [contentsApi.reducerPath]: contentsApi.reducer,
  },
  middleware: () => {
    return getDefaultMiddleware().concat([
      contentsApi.middleware,
      projectsApi.middleware, 
      stackApi.middleware]);
  }
})

