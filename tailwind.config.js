/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        "main-blue": "rgb(10, 99, 222)",
        "main-blue-opacity-30": "rgba(10, 99, 222, .3)",
        "main-blue-opacity-20": "rgba(10, 99, 222, .2)",
      },
      boxShadow: {
        "default": "2px 4px 12px rgba(0, 0, 0, 0.08)",
      },
      fontSize: {
        "h1": "90px",
        "h2": "80px",
        "h3": "",
        "body1": "18px"
      },
      width: {
        "w-screen": "100vw"
      },
      fontFamily: {
        "fira-code": "Fira Code"
      }
    },
  },
  plugins: [],
}
